#!/bin/bash
set -e

ansible-playbook /opt/ansible/basic/playbook.yml

exec gosu "${APP_USER}" /usr/bin/php -f "${APP_USER_HOME}"/daemon/$@